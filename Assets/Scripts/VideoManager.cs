﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoManager : MonoBehaviour
{
    // Video clip array to be accessed to apply visuals to Woodie
    public VideoClip[] excitedVideoClips;
    public VideoClip[] tiredVideoClips;
    public VideoClip[] restVideoClips;
    public VideoClip mutedVideoClip;

    private VideoPlayer videoPlayer;
    public Light pointLight;

    public char state; // Determines current state of woodie, 'e' - excited; 't' - tired; 'r' - rest
    public int vidId; // Current expression that is being play, commented code in Update() should which expression the ID pertains to
    private bool mute; // mute toggle. keyboard button '1' mutes the visuals

    void Awake()
    {
        videoPlayer = GetComponent<VideoPlayer>();
        pointLight = GameObject.Find("Point Light").GetComponent<Light>();
    }
    // Start is called before the first frame update
    void Start()
    {
        // Initial state and expression triggered
        vidId = 0;
        state = 'e';
        PlayVideo(vidId);
        pointLight.intensity = 22.4f;
        mute = false;
    }

    // Update is called once per frame
    //Use key presses to swtich between states; "e" for excited, "t" for tired, "r" for rest.
    //Excited and Tired have 5 expressions each. Rest has 2 expressions
    void FixedUpdate()
    {
        //if (Input.GetKeyDown("1")) // Moving or sleeping (when in resting state)
        //{
        //    vidId = 0;
        //    PlayVideo(0);

        //}
        //else if (Input.GetKeyDown("2")) //Moving awareness or waking up (when in resting state)
        //{
        //    vidId = 1;
        //    PlayVideo(1);
        //}
        //else if (Input.GetKeyDown("3")) // Draw Intent
        //{
        //    vidId = 2;
        //    PlayVideo(2);
        //}
        //else if (Input.GetKeyDown("4")) // Drawing
        //{
        //    vidId = 3;
        //    PlayVideo(3);
        //}
        //else if (Input.GetKeyDown("5")) // Drawing awareness
        //{
        //    vidId = 4;
        //    PlayVideo(4);
        //}
        //else if (Input.GetKeyDown("e")) // go to excited state
        //{
        //    state = 'e';
        //}
        //else if (Input.GetKeyDown("t")) // go to tired state
        //{
        //    state = 't';
        //}
        //else if (Input.GetKeyDown("r"))// go to resting state
        //{
        //    state = 'r';            
        //}


        if (Input.GetKeyDown("1")) // press '1' to toggle mute
        {
            mute = !mute;
            PlayVideo(vidId);
        }

    }

    //Video clips accessed through id number, see Inspector of Cube to see which video is associated with which ID
    public void PlayVideo(int id)
    {
        if (mute == false)
		{
            pointLight.intensity = 22.4f;
            videoPlayer.isLooping = true;
            if (state == 'e')
            {
                videoPlayer.clip = excitedVideoClips[id];
            }
            if (state == 't')
            {
                videoPlayer.clip = tiredVideoClips[id];
            }
            if (state == 'r')
            {
                if (id == 1)
                {
                    videoPlayer.isLooping = false; // don't loop on wake up expression
                    videoPlayer.clip = restVideoClips[id];
                }
                else
                {
                    videoPlayer.clip = restVideoClips[id];
                }

            }
            videoPlayer.Play();
        }
        else // mute toggle on
		{
            pointLight.intensity = 0f;
            videoPlayer.clip = mutedVideoClip;
		}

    }
}
