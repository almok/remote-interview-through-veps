﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExtensionMethods;

public class WoodieMovementByFred : MonoBehaviour
{
    // Behaviour

    [SerializeField] float speed = 1.5f;
    public bool moveWoodie;

    // public vars
    [HideInInspector] public GameObject player;
    [HideInInspector] public GameObject colliderLeft;
    [HideInInspector] public GameObject colliderRight;

    // private vars
    public int directionNew = 0;
    public bool isTurning;
    private float t = 0;
    bool rewind;

    public VideoManager woodieState;
    public AwarenessBehaviour woodieAware;
    // Start is called before the first frame update
    void Start()
    {
        // find and connect gameobjects in scene
        player = GameObject.Find("Player");
        colliderLeft = GameObject.Find("Edge Collider Left");
        colliderRight = GameObject.Find("Edge Collider Right");

        // find video component to check state of woodie (Almo)
        woodieState = GameObject.Find("Cube").GetComponent<VideoManager>();
        woodieAware = GetComponent<AwarenessBehaviour>();

        // recalculate change in direction every 4 secs
        InvokeRepeating("ChangeDirection", 1, 4);

        moveWoodie = true;
        rewind = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (moveWoodie)
        {
            // move forward
            transform.Translate(Vector3.forward * speed);
        }

        // Woodie doesn't turn if in rest state or player is in the way (Almo)
        if (woodieState.state != 'r' || woodieAware.aware == false)
        {
            transform.Rotate(Vector3.up, Mathf.Lerp(0, directionNew, t) * Time.fixedDeltaTime);
        }
        

        // .. and increase the t interpolater

            t += 0.7f * Time.fixedDeltaTime;

        // Woodie stops if in rest state or player is in the way (Almo)
        if (woodieState.state == 'r' || woodieAware.aware == true)
		{
            moveWoodie = false;
		}
        else if (t > 1.0f)
        {
            directionNew = 0;
            t = 0;
            isTurning = false;
            moveWoodie = true;
        }

    }

    // this code runs when woodie touches the invisible boundaries on the sidewalk
    private void OnTriggerEnter(Collider other)
    {
            // did you touch anything in the scene with the tag "boundaries"?
        if (other.CompareTag("Boundaries"))
        {
            // flip 180 degrees
            transform.Rotate(Vector3.up, 180);
        }
    }


void ChangeDirection()
    {
        // Woodie doesn't turn if in rest state or player is in the way (Almo)
        if (woodieState.state != 'r' || woodieAware.aware == false)
        {
            if (!isTurning)
            {
                if (Random.Range(0, 100) < 25) // 25% chance of break
                {
                    moveWoodie = false;
                }
                else // otherwise set random direction, turn woodie, move woodie
                {
                    moveWoodie = false;

                    directionNew = Random.Range(-6, 6) * 20;

                    isTurning = true;

                }
            }
        }
    }
}
