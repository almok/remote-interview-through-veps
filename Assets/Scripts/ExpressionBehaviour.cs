﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpressionBehaviour : MonoBehaviour
{
    public VideoManager woodieVideo;
    public AudioManager woodieAudio;
    public AwarenessBehaviour woodieAware;

    private int currentExpression;
    // Start is called before the first frame update
    void Start()
    {
        // find video and audio components to change expression
        woodieVideo = GameObject.Find("Cube").GetComponent<VideoManager>();
        woodieAudio = GameObject.Find("Woodie_top_plate").GetComponent<AudioManager>();
        woodieAware = GetComponent<AwarenessBehaviour>();

        //Change expression every 10 seconds
        InvokeRepeating("ChangeExpression", 1, 10);
    }

    //Expression will change every 10 seconds
    void ChangeExpression()
	{
        currentExpression = woodieVideo.vidId; // what is the current expression? this determines what will come next
        if (woodieAware.aware == false) // only change state if nobody is infront of woodie (simply a safety check)
		{
            if (woodieVideo.state == 'e' || woodieVideo.state == 't') // change between expression
            {
                if (currentExpression == 0)
                {
                    woodieVideo.vidId = 2;
                }
                else if (currentExpression == 2)
                {
                    woodieVideo.vidId = 3;
                }
                else if (currentExpression == 3)
                {
                    ChangeState(); // when it reached the last expression in state, change state
                    woodieVideo.vidId = 0;
                }
                //woodieVideo.PlayVideo(woodieVideo.vidId);
                //woodieAudio.PlayAudio(woodieVideo.vidId);
            }
            else if (woodieVideo.state == 'r')
            {
                if (currentExpression == 0)
                {
                    woodieVideo.vidId = 1;
                }
                else if (currentExpression == 1)
                {
                    ChangeState();
                    woodieVideo.vidId = 0;
                }
                //woodieVideo.PlayVideo(woodieVideo.vidId);
                //woodieAudio.PlayAudio(woodieVideo.vidId);
            }
            woodieVideo.PlayVideo(woodieVideo.vidId);
            woodieAudio.PlayAudio(woodieVideo.vidId);
        }
    }

    // Change state when at the last expression of current state. States go from excited to tired to rest and back.
    void ChangeState()
	{
        if (woodieVideo.state == 'e')
		{
            woodieVideo.state = 't';
		}
        else if (woodieVideo.state == 't')
		{
            woodieVideo.state = 'r';
		}
        else if (woodieVideo.state == 'r')
		{
            woodieVideo.state = 'e';
		}
	}
}
