﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExtensionMethods;

public class WoodieMovement : MonoBehaviour
{
    // Behaviour

    [SerializeField] float speed = 1.5f;
    [SerializeField] bool followsPlayer;

    // public vars
    public GameObject player;

	// private vars
	private float xDirection = 0;
    private float zDirection = 0;

    public Vector3 target;
    public Vector3 distanceToPlayer;

    public float xTest;
    public float zTest;

	private Quaternion newDirection;

	// Start is called before the first frame update
	void Start()
    {
           // find player gameobject
        player = GameObject.Find("Player");

            // recalculate change in direction every 3 secs
        InvokeRepeating("ChangeDirection", 1, 3);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
            // woodie eithers follows the player or just moves around randomly
        if (followsPlayer)
        {
                // calculate vector between woodie and player
            distanceToPlayer = player.transform.position - transform.position;

                // no vertical movement pls
            distanceToPlayer.y = 0;

                // move woodie
            transform.Translate(distanceToPlayer.normalized * speed);
        }
        else
        {
            // random movement
            target = new Vector3(xDirection, 0, zDirection);
            transform.Translate(target.normalized * speed);
            //print(target.x);
            //print(target.z);
            //print(target.normalized);
		}

		Vector3 testTarget = new Vector3(xTest, 0, zTest);
		int offset = calculateOffset(testTarget);
		print(offset);



		//transform.rotation = Quaternion.Euler(0, target.y, 0);

		//float singleStep = 2f * Mathf.PI * Time.deltaTime;

		//Vector3 newDirection = Vector3.RotateTowards(transform.forward, target, singleStep, 0.0f);
		////Vector3 newDirection = Vector3.RotateTowards(transform.forward, distanceToPlayer, singleStep, 0.0f);

		//transform.rotation = Quaternion.LookRotation(newDirection);
		//Debug.DrawRay(transform.position, newDirection , Color.red);

		//newDirection = Quaternion.LookRotation(target.normalized);

		//transform.rotation = Quaternion.Slerp(transform.rotation, newDirection, singleStep);

	}

	void ChangeDirection()
    {
        // move inwards if too close to sidewalk
        if (transform.position.x <= -250 || transform.position.x >= 250)
        {
            xDirection = 0.0f - transform.position.x;
            zDirection = Random.Range(-20, 20);
        }
        else if (Random.Range(0, 100) < 25) // 25% chance of break
        {
            xDirection = 0;
            zDirection = 0;
        }
        else // otherwise random direction
        {
            xDirection = Random.Range(-20, 20);
            zDirection = Random.Range(-20, 20);
        }
    }

    // Calculate offset to be applied to visuals. This offset determines where Woodie is looking
	public int calculateOffset(Vector3 target)
	{
		float x = target.normalized.x;
		float z = target.normalized.z;
		float angle = Mathf.Abs(Mathf.Atan(z / x) * Mathf.Rad2Deg);
		print(angle);
		int offset = 0;

        // Quadrant 1, angles between 0-90 degrees mapped to 0-3 offset
		if (x >= 0 && z >= 0)
		{
			offset = map(0f, 90f, 0f, 3f, angle);
		}
		if (x < 0 && z >= 0)
		{
            offset = map(0f, 90f, 7f, 4f, angle);
		}
        if (x >= 0 && z < 0)
        {
            offset = map(0f, 90f, 15f, 12f, angle);
        }
        if (x < 0 && z < 0)
        {
            offset = map(0f, 90f, 8f, 11f, angle);
        }

        return offset;
	}

	public int map(float oldMin, float oldMax, float newMin, float newMax, float oldValue)
	{
		float oldRange = (oldMax - oldMin);
		float newRange = (newMax - newMin);
		float newValue = (((oldValue - oldMin) * newRange) / oldRange) + newMin;

		int offset = Mathf.RoundToInt(newValue);

		return offset;
	}
}
