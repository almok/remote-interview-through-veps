﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AwarenessBehaviour : MonoBehaviour
{
    public GameObject player;
    public VideoManager videoManager;
    public AudioManager audioManager;

    public float angle;
    public float distance;
    
    private Vector3 directionToWoodie;
    private int awarenessExpression;
    private int currentExpression;
    public bool aware;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        videoManager = GameObject.Find("Cube").GetComponent<VideoManager>();
        audioManager = GameObject.Find("Woodie_top_plate").GetComponent<AudioManager>();
        aware = false;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Retrieve current expression to revert to after player has moved out of the way
        currentExpression = videoManager.vidId;
        print(currentExpression);

        // Calculate angle and distance of player from Woodie
        directionToWoodie = transform.position - player.transform.position;
        angle = Vector3.Angle(transform.forward, directionToWoodie);
        distance = directionToWoodie.magnitude;

        // Awareness only occurs in excited or tired state.
        if (videoManager.state == 'e' || videoManager.state == 't')
        {
            // if at the front of woodie go to awareness state
            if (angle >= 130 && angle < 180 && distance < 300)
            {
                // activate only once
                if (aware == false)
                {
                    if (currentExpression == 0 || currentExpression == 2) awarenessExpression = 1;
                    if (currentExpression == 3) awarenessExpression = 4;
                    videoManager.PlayVideo(awarenessExpression);
                    audioManager.PlayAudio(awarenessExpression);
                    aware = true;
                }
            }


            // revert to current expression when player moves out of the way
            else
            {
                videoManager.PlayVideo(currentExpression);
                aware = false; // reset to false so awareness may be activated again
            }
        }
        
	}
}
