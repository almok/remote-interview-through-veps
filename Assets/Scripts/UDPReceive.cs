﻿using UnityEngine;
using System.Collections;

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

public class UDPReceive : MonoBehaviour
{

    // receiving Thread
    Thread receiveThread;

    // udpclient object
    UdpClient receiveClient;

    //TCP
    TcpClient mySocket;
    NetworkStream theStream;
    StreamWriter theWriter;
    Thread clientReceiveThread;

    // public
    public int receivePort; // define > init
    //public int sendPort; // define > init

    public int numLEDs;

    LEDControl led;

    // infos
    public string lastReceivedUDPPacket = "";
    public string allReceivedUDPPackets = ""; // clean up this from time to time!


    // start from shell
    private static void Main()
    {
        UDPReceive receiveObj = new UDPReceive();
        receiveObj.init();

        string text = "";
        do
        {
            text = Console.ReadLine();
        }
        while (!text.Equals("exit"));

    }
    // start from unity3d
    public void Start()
    {

        init();
    }


    // init
    private void init()
    {
        // Endpunkt definieren, von dem die Nachrichten gesendet werden.
        print("UDPSend.init()");

        led = GameObject.Find("Woodie_LEDs").GetComponent<LEDControl>();

        // define port
        receivePort = 6454;
        //sendPort = 6455;

        numLEDs = 64;

        receiveClient = new UdpClient(receivePort);

        receiveThread = new Thread(
            new ThreadStart(() => ReceiveData(receiveClient)));
        receiveThread.IsBackground = true;
        receiveThread.Start();



    }

    // receive thread
    private void ReceiveData(UdpClient client)
    {

        while (true)
        {

            try
            {

                // Bytes empfangen.
                IPEndPoint anyIP = new IPEndPoint(IPAddress.Any, 0);

                byte[] received = client.Receive(ref anyIP);


                // Bytes mit der UTF8-Kodierung in das Textformat kodieren.
                string text = Encoding.UTF8.GetString(received);

                if (text.Contains("Art-Net"))
                {

                    int dmx = 18;
                    StringBuilder[] sb = new StringBuilder[numLEDs];
                    String[] str = new String[numLEDs];

                    for (int i = 0; i < numLEDs; i++)
                    {
                        sb[i] = new StringBuilder();
                        sb[i].Append("#");
                        sb[i].Append(received[dmx++].ToString("X2"));
                        sb[i].Append(received[dmx++].ToString("X2"));
                        sb[i].Append(received[dmx++].ToString("X2"));
                        str[i] = sb[i].ToString();

                    }

                    //Output the colors received color values to the Car's LEDs
                    led.ChangeColor(str);

                }
            }
            catch (Exception err)
            {
                print(err.ToString());
                break;
            }
        }
    }

    private string ExtractString(byte[] packet, int start, int length)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < packet.Length; i++)
        {
            sb.Append((char)packet[i]);
        }
        return sb.ToString();
    }


    private void OnDisable()
    {
        // make sure to clean up sockets on exit
        receiveClient.Client.Close();
        receiveClient.Close();
    }
}