﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodieAudio : MonoBehaviour
{
    #region public vars

    [SerializeField] float distanceToPlayer;
    [SerializeField] float velocity;

    [SerializeField] float triggerAtDistance;

    [SerializeField] AudioClip clipOneShot;


    AudioSource audioSourceMotor;
    AudioSource audioSourceOneShot;

    #endregion

    #region private vars

    

    #endregion


    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
            // assign Gameobject Player to local variable player
        player = GameObject.Find("Player");

        audioSourceMotor = GetComponent<AudioSource>();
        //InvokeRepeating("TriggerSoundFile", 1, 1);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
            // calculate distance to player
        distanceToPlayer = Vector3.Distance(transform.position, player.transform.position);
    }

    void TriggerSoundFile()
    {
        //audioSource.clip = audioClip;
        //audioSourceMotor.PlayOneShot(clipOneShot);
        //Instantiate<AudioSource>(audioSource);
    }


    //// fancy way of adding audio sources on the fly

    //// define the audio clips
    //public AudioClip clipAmb;
    //public AudioClip clipEngine;
    //public AudioClip clipWeapon1;
    //public AudioClip clipWeapon2;

    //private AudioSource audioAmb;
    //private AudioSource audioEngine;
    //private AudioSource audioWeapon1;
    //private AudioSource audioWeapon2;

    //public AudioSource AddAudio(AudioClip clip, bool loop, bool playAwake, float vol)
    //{

    //    AudioSource newAudio = gameObject.AddComponent<AudioSource>();

    //    newAudio.clip = clip;
    //    newAudio.loop = loop;
    //    newAudio.playOnAwake = playAwake;
    //    newAudio.volume = vol;

    //    return newAudio;

    //}

    //public void Awake()
    //{
    //    // add the necessary AudioSources:
    //    audioAmb = AddAudio(clipAmb, true, true, 0.2);
    //    audioEngine = AddAudio(clipEngine, true, true, 0.4);
    //    audioWeapon1 = AddAudio(clipWeapon1, false, false, 0.8);
    //    audioWeapon2 = AddAudio(clipWeapon2, false, false, 0.8);
    //}

}
