﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class AudioManager : MonoBehaviour
{
    // Audio clip array to be accessed to apply audio to Woodie
    public AudioClip[] excitedAudioClips;
    public AudioClip[] tiredAudioClips;
    public AudioClip[] restAudioClips;


    private AudioSource audioSource;

    public VideoManager woodieVideo; //Refer to VideoManager to obtain state info
    private char state; // Determines current state of woodie, 'e' - excited; 't' - tired; 'r' - rest
    private bool mute; // mute toggle. keyboard button '2' mutes the audio

    void Awake()
	{
        audioSource = gameObject.GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        woodieVideo = GameObject.Find("Cube").GetComponent<VideoManager>();
        audioSource.volume = 0.467f;
        mute = false;
    }

    // Update is called once per frame
    //Use key presses to swtich between states; "e" for excited, "t" for tired, "r" for rest.
    //Excited has 4 expressions, Tired has 5, Rest has 2
    void FixedUpdate()
    {
        
        //if (Input.GetKeyDown("1")) // moving or sleeping (when in resting state)
        //{
        //    PlayAudio(0);
        //}
        //else if (Input.GetKeyDown("2")) // moving awareness or waking up (when in resting state)
        //{
        //    PlayAudio(1);
        //}
        //else if (Input.GetKeyDown("3")) // draw intent
        //{
        //    PlayAudio(2);
        //}
        //else if (Input.GetKeyDown("4")) // drawing
        //{
        //    PlayAudio(3);
        //}
        //else if (Input.GetKeyDown("5")) // drawing awareness
        //{
        //    PlayAudio(4);
        //}

        if (Input.GetKeyDown("2")) // press '2' to toggle mute
        {
            mute = !mute;
		}
    }

    //Audio clips accessed through id number, see Inspector of Woodie_top_plate to see which sound is associated with which ID
    public void PlayAudio(int id)
	{
        state = woodieVideo.state;
        if (mute == false)
		{
            audioSource.volume = 0.467f;
            if (state == 'e')
            {
                audioSource.clip = excitedAudioClips[id];
            }
            if (state == 't')
            {
                audioSource.clip = tiredAudioClips[id];
            }
            if (state == 'r' && id <= 1) // rest only has two audio clips
            {
                audioSource.clip = restAudioClips[id];
            }
            audioSource.Play();
        }
		else // mute toggle on
		{
            audioSource.Stop();
            audioSource.volume = 0.0f;
		}
	}
}
