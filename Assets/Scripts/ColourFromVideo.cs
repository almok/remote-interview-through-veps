﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Text;
using ExtensionMethods;

public class ColourFromVideo : MonoBehaviour
{
    public RenderTexture rTex;
    Texture2D videoFrame;

    public int numLEDs;

    LEDControl led;

    // Start is called before the first frame update
    void Start()
    {

        led = GameObject.Find("Woodie_LEDs").GetComponent<LEDControl>();

        numLEDs = 64;
    }

    // Update is called once per frame
    void Update()
    {
        videoFrame = rTex.toTexture2D(); //extension method to convert Render Texture to Texture2D so colours may be read

		StringBuilder[] sb = new StringBuilder[numLEDs];
		String[] str = new String[numLEDs];
		int i = 0;


        //Read in every pixel from 16x4 video & send string in hex code format LED control script
		for (int y = 0; y < videoFrame.height; y++)
		{
            for(int x = 0; x < videoFrame.width; x++)
			{
                Color pix = videoFrame.GetPixel(x, y); // retrieve pixel colour at x-y coordinate

                String colorStr = ColorUtility.ToHtmlStringRGB(pix); // convert color retrieved to rgb representation
                string redStr = colorStr.Substring(0, 2); // red
                string greenStr = colorStr.Substring(2, 2); // blue
                string blueStr = colorStr.Substring(4, 2); // green


                // build collected rgb values into string with leading #
                sb[i] = new StringBuilder();
                sb[i].Append("#");
                sb[i].Append(redStr);
                sb[i].Append(greenStr);
                sb[i].Append(blueStr);
                str[i] = sb[i].ToString();
                i++;
            }
		}
        led.ChangeColor(str); // send to LEDControl script
    }


}
