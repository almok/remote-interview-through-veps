﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LEDControl : MonoBehaviour
{

    Material m_Material;
    public Material mat;

    bool lockThread = false;
    string str = "";
    string oldStr = "";
    string[] strings;

    // Start is called before the first frame update
    void Start()
    {
        Renderer[] children;
        children = GetComponentsInChildren<Renderer>();
        foreach (Renderer rend in children)
        {
            var mats = new Material[rend.materials.Length];
            for (var j = 0; j < rend.materials.Length; j++)
            {
                //print(j + " " + rend.materials[j].name);
                Material m = Material.Instantiate(mat);
                m.SetColor("_Color", new Color(Random.Range(0,1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f)));
                m.SetColor("_EmissionColor", new Color(Random.Range(0, 1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f)));
                m.EnableKeyword("_EMISSION");
                m.name = rend.materials[j].name;
                mats[j] = m;
            }
            rend.materials = mats;
        }
    }

    // Update is called once per frame
    void Update()
    {
    
        if (!lockThread)
        {
            lockThread = true;
            //quick fix to avoid flickering of the LEDs (if we later send dynamic light patterns, we should check for each single LED if the value changed to avoid flickering
            if (str != oldStr)
            {

                Renderer[] children;
                children = GetComponentsInChildren<Renderer>();
                foreach (Renderer rend in children)
                {
                    for (var j = 0; j < rend.materials.Length; j++)
                    {

                        if (j < strings.Length)
                        {
                            
                            int num = int.Parse(rend.materials[j].name.Split(' ')[0].Split('D')[1]);
                            Color newCol;
                            ColorUtility.TryParseHtmlString(strings[num-1], out newCol);
                            rend.materials[j].SetColor("_Color", newCol);
                            rend.materials[j].SetColor("_EmissionColor", newCol);
                           
                        }
                    
                    }
                }

                oldStr = str;
            }
            lockThread = false;
        }
    }

    public void ChangeColor(string[] stringsSend)
    {

        if (!lockThread)
        {
            lockThread = true;
            str = "";
            for (int i = 0; i < stringsSend.Length; i++)
            {
                str += stringsSend[i];
            }
            strings = stringsSend;
            lockThread = false;
        }

    }
}
